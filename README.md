# Ansible Model
This project teach how to use Ansible with vagrant and VirtualBox in a linux machine.



## How to Install Ansible 

1. Open your terminal of commands and type
    ```bash
    sudo apt install ansible
    ```

2. Verify the Ansible version
    ```bash
    ansible --version
    ```

3. Download the vagrant
    ```bash
    curl 'https://releases.hashicorp.com/vagrant/2.3.4/vagrant_2.3.4-1_i686.deb' --output vagrant.deb
    ```

4. Install vagrant
    ```bash
    sudo dpkg -i vagrant.deb
    ```

5. Verify the vagrant version
    ```bash
    vagrant -v
    ```

6. Download VirtualBox
    ```bash
    curl 'https://download.virtualbox.org/virtualbox/7.0.6/virtualbox-7.0_7.0.6-155176~Debian~buster_amd64.deb' --output virtual-box.deb
    ```

7. Install VirtualBox
    ```bash
    sudo dpkg -i virtual-box.deb
    ```
8. Finish! The environment is done! The next step will be, to create the configurations.

## How to up the machine

1. Now is necessery to create the 'Vagrantfile' with the configurations. To use the 'Vagrantfile' inside that project, set the configurations and save the modifications.

    ![Imgur](docs/images/img-vagrantfile.jpg)

2. Execute the command below in the root that project, to create a new vagrant environment.
    ```bash
    vagrant init
    ```

3. Execute the command below in the same level the 'Vagrantfile', to up the machine.
    ```bash
    vagrant up
    ```

## How to enter in machine via ssh

1. Define in the file 'hosts' in this project, the IP numbers the machines that was created.
   
    ![Imgur](docs/images/img-hosts.jpg)

2. Now, enter in the machine that was created previously. For this, execute the command bellow.
    ```bash
    vagrant ssh
    ```
3. You will see a picture like this.

    ![Imgur](docs/images/img-inside-machine.jpg)

## Hello World in Ansible

1. Execute the command bellow.
    ```bash
    ansible wordpress -u vagrant --private-key .vagrant/machines/wordpress/virtualbox/private_key -i hosts -m shell -a 'echo Hello, World'
    ```
2. You will see a picture like this.

    ![Imgur](docs/images/img-hello.jpg)